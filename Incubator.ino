const int door_pin = 12;
const int vTHRESH = A4;
const int vT1 = A2;
const int vT2 = A3;
const int heat = 4;
const int alarmLED = 2;

int heatState;
int alarmState;
int doorState;

float volt1;
float volt2;
float voltTHRESH;





void setup() {
  Serial.begin(9600);
  pinMode(door_pin, INPUT_PULLUP);
  pinMode(vTHRESH, INPUT);
  pinMode(vT1, INPUT);
  pinMode(vT2, INPUT);
  pinMode(alarmLED, OUTPUT);
  pinMode(heat, OUTPUT);
}

void alarm() {
  volt1 = analogRead(vT1);
  volt1 = (volt1/1024.0)*5.0;
  
  volt2 = analogRead(vT2);
  volt2 = (volt2/1024.0)*5.0;
  
  voltTHRESH = analogRead(vTHRESH);
  voltTHRESH = (voltTHRESH/1024.0)*5.0;
  
  doorState = digitalRead(door_pin);
  
  if ((doorState == LOW) || (volt1 > (voltTHRESH + 0.2) || volt2 > (voltTHRESH + 0.2)) || (abs(volt1 - volt2) > .2*volt1) || (abs(volt1 - volt2) > .2*volt2)) {
    digitalWrite(alarmLED, HIGH);
    digitalWrite(heat, LOW);
    alarmState = HIGH;
    
  }
  else{
    digitalWrite(alarmLED, LOW);
     alarmState = LOW;
    
  }
}

void warming() {
  

  if (alarmState == LOW && (volt1 < voltTHRESH && volt2 < voltTHRESH)) {
    digitalWrite(alarmLED, LOW);
    digitalWrite(heat, HIGH);
    
  }
}

void cooling() {
  
  
  if ((volt1 + volt2)/2.0 > voltTHRESH + 0.2) {
  
      digitalWrite(alarmLED, HIGH);
      digitalWrite(heat, LOW);
  }
  
}

void loop() {
  // put your main code here, to run repeatedly:

  alarm();

  warming();

  cooling();
    
  delay(50);
  
}